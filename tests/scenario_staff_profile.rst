======================
Staff profile scenario
======================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> from decimal import Decimal
    >>> today = datetime.date.today()


Install stock_unit_load_quality_control Module::

    >>> config = activate_modules(['staff_profile', 'timesheet_cost_info'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create profile and prices::

    >>> Profile = Model.get('staff.profile')
    >>> profile = Profile(name='Profile 1', company=company)
    >>> cost_prices = [datetime.date(2011, 1, 1), Decimal(10), Decimal(15)]
    >>> profile_price = profile.payroll_prices.new()
    >>> profile_price.profile = profile
    >>> profile_price.date = cost_prices[0]
    >>> profile_price.cost_price = cost_prices[1]
    >>> profile_price.payroll_price = cost_prices[2]
    >>> profile.save()

Create timesheet lines::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> party = Party(name='Empleado 1')
    >>> party.save()
    >>> employee = Employee(company=company, party=party)
    >>> employee.profile = profile
    >>> employee.save()
    >>> price = employee.cost_prices.new()
    >>> price.date = today
    >>> price.cost_price = Decimal('8.50')
    >>> price.payroll_price = Decimal('7.23')
    >>> employee.save()
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> line = Line(employee=employee, date=today, work=work, duration=datetime.timedelta(hours=8))
    >>> line.save()
    >>> line.payroll_cost_price
    Decimal('7.23')
    >>> line.payroll_cost == line.payroll_cost_price * Decimal(str(line.duration.total_seconds() / 3600))
    True

Check amount field::

    >>> HoursEmployee = Model.get('timesheet.hours_employee')
    >>> hours_employee, = HoursEmployee.find([])

Check amount field::

    >>> WeeklyEmployee = Model.get('timesheet.hours_employee_weekly')
    >>> weekly_employee, = WeeklyEmployee.find([])
    >>> weekly_employee.payroll_amount == line.payroll_cost_price * Decimal(str(line.duration.total_seconds() / 3600))
    True