# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import profile
from . import timesheet
from . import time_clock


def register():
    Pool.register(
        profile.Profile,
        profile.ProfilePayrollCost,
        timesheet.Timesheet,
        profile.Employee,
        profile.EmployeeCostPrice,
        module='staff_profile', type_='model')
    Pool.register(
        time_clock.TimeClockPrintStart,
        module='staff_profile', type_='model',
        depends=['company_time_clock'])
    Pool.register(
        time_clock.TimeClockPrint,
        module='staff_profile', type_='wizard',
        depends=['company_time_clock'])
    Pool.register(
        timesheet.TimesheetCostInfo,
        timesheet.HoursEmployee,
        timesheet.HoursEmployeeWeekly,
        timesheet.HoursEmployeeMonthly,
        module='staff_profile', type_='model',
        depends=['timesheet_cost_info'])
