# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime

from decimal import Decimal
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.pool import Pool
from trytond.modules.company.tests import create_company, set_company


class StaffProfileTestCase(ModuleTestCase):
    """Test module"""
    module = 'staff_profile'
    extras = [
        'timesheet_cost_info',
        'company_time_clock',
        'timesheet_absolute_hours'
        ]

    @with_transaction()
    def test_compute_profile_prices(self):
        """Test staff profile"""
        pool = Pool()
        Party = pool.get('party.party')
        Profile = pool.get('staff.profile')
        ProfilePrice = pool.get('staff.profile.payroll_price')
        Employee = pool.get('company.employee')

        cost_prices = [
            (datetime.date(2011, 1, 1), Decimal(10), Decimal(15)),
            (datetime.date(2012, 1, 1), Decimal(15), Decimal(20)),
            (datetime.date(2013, 1, 1), Decimal(20), Decimal(25)),
            ]
        test_prices = [
            (datetime.date(2010, 1, 1), 0, 0),
            (datetime.date(2011, 1, 1), Decimal(10), Decimal(15)),
            (datetime.date(2011, 6, 1), Decimal(10), Decimal(15)),
            (datetime.date(2012, 1, 1), Decimal(15), Decimal(20)),
            (datetime.date(2012, 6, 1), Decimal(15), Decimal(20)),
            (datetime.date(2013, 1, 1), Decimal(20), Decimal(25)),
            (datetime.date(2013, 6, 1), Decimal(20), Decimal(25)),
            ]
        party = Party(name='Pam Beesly')
        party.save()
        company = create_company()
        profile = Profile(name='Profile 1', company=company)
        profile.save()
        with set_company(company):
            employee = Employee(party=party.id, company=company)
            employee.profile = profile
            employee.save()
            for date, cost_price, payroll_price in cost_prices:
                ProfilePrice(
                    profile=profile,
                    date=date,
                    cost_price=cost_price,
                    payroll_price=payroll_price).save()
            for date, cost_price, payroll_price in test_prices:
                self.assertEqual(employee.compute_cost_price(date),
                    cost_price)
                self.assertEqual(employee.compute_payroll_price(date),
                    payroll_price)


del ModuleTestCase
